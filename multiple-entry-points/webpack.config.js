const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
  entry:{
    home:  path.resolve(__dirname, 'src/js/index.js'),
    prices:  path.resolve(__dirname, 'src/js/prices.js'),
    contact:  path.resolve(__dirname, 'src/js/contact.js')
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'js/[name].js',
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        // use: [
        //   { loader: "style-loader" }, // Agrega el css al DOM en un <style>
        //   { loader: "css-loader" }, // interpreta los archivos css en js via import
        // ]
        use: ExtractTextPlugin.extract({
          // [ 'style-loader', 'css-loader' ]
          // fallback: 'style-loaer',
          use: 'css-loader'
        })
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin("css/[name].css") // Allows the new file name to be created from the original
    // new ExtractTextPlugin("css/styles.css")
  ]
}