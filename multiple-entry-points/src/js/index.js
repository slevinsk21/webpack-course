import '../css/estilos.css'

console.log("Mutiple Entry Points")

function test() {
  let allParas = document.getElementsByTagName('p')
  let num = allParas.length

  alert(`Hay ${num} <p> elementos en este documento`)
}

test()

document.write('Hola mundo! este es el index (configurando multiples entry points)');