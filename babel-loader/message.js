import renderToDOM from './render-to-dom.js'
import makeMessage from './make-message.js';

const waitTime = new Promise((resolve, reject) => {
    setTimeout(() => resolve('after 3seg'), 3000)
})

module.exports = {
    firstMessage: "Hi world from a module",
    delayedMessage: async () => {
        const message = await waitTime
        // const element = document.createElement("p")
        // element.textContent = message
        renderToDOM(makeMessage(message))
    }
}