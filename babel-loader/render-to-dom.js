import makeMessage from './make-message.js'

const waitTime = new Promise((resolve, reject) => {
    setTimeout(() => resolve('3seg'), 3000)
})

module.exports = function renderToDOM(element) {
    document.body.append(element)
}